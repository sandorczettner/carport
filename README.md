# Carport with NodeMCU

This arduino code is used on my carport to turn on the lights and send sensor informations to my home automation running Domoticz.

I hope it will be useful for someone else as well.

The temperature sensor is a DHT-22 sensor and it's using a HC-SR501 PIR sensor for movement to detect people passing the pavement. The two gate sensors are MC-38 magnetic sensors hooked up with 10kOhm pull-up resistors.

The contents of `src/secrets.h` is in the following format:

```
#define SECRET_SSID "Your SSID"
#define SECRET_PASS "wifi-password"
#define SECRET_AUTH "Authorization: Basic xxxxxxxxxxxxxxxxxxxx"
#define SECRET_HOST "192.168.0.102" // Domoticz Host
```

## Licence

Copyright 2019 Sandor Czettner

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.