#include <SPI.h>
#include <Adafruit_Sensor.h>
#include <DHT.h>
#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <time.h>
#include <stdlib.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>
#include <secrets.h>

String rounding(float var);

/* SSID & Password */
const char* ssid = SECRET_SSID;
const char* password = SECRET_PASS;

const String domoticzAuth = SECRET_AUTH;
const String domoticzHost = SECRET_HOST;
const int domoticzPort = 8080;

const int readDelay = 5000; // 5 seconds
const int gateDelay = 300; // gate sensor triggering delay

ESP8266WebServer server(80);

#define DHTTYPE DHT22        // DHT22 sensor is used
#define DHTPIN  D4           // DHT22 data pin is connected to NodeMCU pin D4
#define RELAY1  D0
#define RELAY2  D1
#define RELAY3  D2
#define RELAY4  D3
#define GATE1   D5
#define GATE2   D6
#define PIR1    D7
#define PIR2    D8

DHT dht22(DHTPIN, DHTTYPE);

float temperature = 0;
float humidity = 0;
int timeSinceLastRead = 0;
int timeSinceGate = 0;
int lastInterrupt = 0;

void ICACHE_RAM_ATTR pir1_interrupt(){
  Serial.println("interrupt PIR1");
  lastInterrupt = PIR1;
}
void ICACHE_RAM_ATTR pir2_interrupt(){
  Serial.println("interrupt PIR2");
  lastInterrupt = PIR2;
}
void ICACHE_RAM_ATTR gate1_interrupt(){
  if (timeSinceGate > gateDelay) {
    Serial.println("interrupt GATE1");
    lastInterrupt = GATE1;
    timeSinceGate = 0;
  }
}
void ICACHE_RAM_ATTR gate2_interrupt(){
  if (timeSinceGate > gateDelay) {
    Serial.println("interrupt GATE2");
    lastInterrupt = GATE2;
    timeSinceGate = 0;
  }
}

// Try and connect using a WiFiClientBearSSL to specified host:port and dump HTTP response
void fetchURL(String host, const uint16_t port, String path) {
  WiFiClient client;
  if (!path) {
    path = "/";
  }

  if (client.connect(host, port))
  { 
    client.print("GET " + path + " HTTP/1.1\r\n");
    client.print("Host: " + host + ":" + String(port) + "\r\n");
    client.print(domoticzAuth + "\r\n");
    client.print("Connection: close\r\n\r\n");

    Serial.print("Host: " + host + ":" + String(port) + "\n");
    Serial.println(path);

    delay(100);

    while(client.available()) {
      char c = client.read();
      Serial.print(c);
    }
    Serial.println("");
  }
  client.stop();
}

void notifyDomoticz(int pin) {
  int val = 0;
  val = digitalRead(pin);
  String host, path, station, cmd;
  cmd = "On";
  if (pin == PIR1) {
    station = "501";
  }
  if (pin == PIR2) {
    station = "500";
  }
  if (pin == GATE1) {
    station = "502";
  }
  if (pin == GATE2) {
    station = "503";
  }
  if (pin == GATE1 || pin == GATE2) {
    if (val == HIGH) {
      cmd = "Off";
    } else {
      cmd = "On";
    }
  }
  
  path = "/json.htm?type=command&param=switchlight&idx=" + station + "&switchcmd=" + cmd;
  fetchURL(domoticzHost, domoticzPort, path);
}

void initRelay(int relay) {
  pinMode(relay, OUTPUT);
  digitalWrite(relay, LOW);
}

void setRelay(int pin, String value) {
  if (value == "1") {
    digitalWrite(pin, HIGH);
  } else {
    digitalWrite(pin, LOW);
  }
}

void handle_OnConnect() {
  String ptr = "{";
  ptr += "  \"temperature\": " + rounding(temperature) + ",";
  ptr += "  \"humidity\": " + rounding(humidity) + "";
  ptr += "}";
  server.send(200, "application/json", ptr);
}

void handle_Switch() {
  String device = "", value = "";
  for (uint8_t i = 0; i < server.args(); i++) {
    if (server.argName(i) == "device") {
      device = server.arg(i);
    }
    if (server.argName(i) == "value") {
      value = server.arg(i);
    }
  }

  if (strcmp(device.c_str(), "relay1") == 0)  {
    setRelay(RELAY1, value);
  } else if (strcmp(device.c_str(), "relay2") == 0) {
    setRelay(RELAY2, value);
  } else if (strcmp(device.c_str(), "relay3") == 0) {
    setRelay(RELAY3, value);
  } else if (strcmp(device.c_str(), "relay4") == 0) {
    setRelay(RELAY4, value);
  } else {
    server.send(200, "text/plain", "no device specified");
    return;
  }
  server.send(200, "text/plain", "OK");
}

void handle_NotFound(){
  server.send(404, "text/plain", "Not found");
}

String rounding(float var) 
{ 
  // we use array of chars to store number 
  // as a string. 
  char str[40];  
  
  // Print in string the value of var  
  // with two decimal point 
  sprintf(str, "%.2f", var); 
  return str;
}

void setup() {
  Serial.begin(115200);
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password); //begin WiFi connection
  // Wait for serial to initialize.
  while(!Serial) { }
 
  // Wait for connection
  Serial.println();
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.mode(WIFI_STA);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  
  // start the WiFi OTA library with internal (flash) based storage
    ArduinoOTA.onStart([]() {
    Serial.println("Start OTA");
  });
  ArduinoOTA.onEnd([]() {
    Serial.println("\nEnd OTA");
  });
  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    Serial.printf("OTA Progress: %u%%\r", (progress / (total / 100)));
  });
  ArduinoOTA.onError([](ota_error_t error) {
    Serial.printf("OTA Error[%u]: ", error);
    if (error == OTA_AUTH_ERROR) Serial.println("OTA Auth Failed");
    else if (error == OTA_BEGIN_ERROR) Serial.println("OTA Begin Failed");
    else if (error == OTA_CONNECT_ERROR) Serial.println("OTA Connect Failed");
    else if (error == OTA_RECEIVE_ERROR) Serial.println("OTA Receive Failed");
    else if (error == OTA_END_ERROR) Serial.println("OTA End Failed");
  });
  ArduinoOTA.begin();

  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
  
  server.on("/", handle_OnConnect);
  server.on("/switch", handle_Switch);
  server.onNotFound(handle_NotFound);
  server.begin();
  Serial.println("HTTP server started");

  // Temperature sensor
  dht22.begin(75);

  // Relay
  initRelay(RELAY1);
  initRelay(RELAY2);
  initRelay(RELAY3);
  initRelay(RELAY4);

  // Interrupts used for PIR and gate sensors
  pinMode(PIR1, INPUT_PULLUP);
  pinMode(PIR2, INPUT_PULLUP);
  pinMode(GATE1, INPUT_PULLUP);
  pinMode(GATE2, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(PIR1), pir1_interrupt, RISING);
  attachInterrupt(digitalPinToInterrupt(PIR2), pir2_interrupt, RISING);
  attachInterrupt(digitalPinToInterrupt(GATE1), gate1_interrupt, CHANGE);
  attachInterrupt(digitalPinToInterrupt(GATE2), gate2_interrupt, CHANGE);
}

void loop() {
  if (lastInterrupt != 0) {
    notifyDomoticz(lastInterrupt);
    lastInterrupt = 0;
  }

  ArduinoOTA.handle();
  
  server.handleClient();
  
  if(timeSinceLastRead > readDelay) {
    humidity = dht22.readHumidity();
    temperature = dht22.readTemperature();

    if (isnan(humidity) || isnan(temperature)) {
      Serial.println("Failed to read from DHT sensor!");
    } else {
      Serial.print("Humidity: ");
      Serial.print(humidity);
      Serial.print(" %\t");
      Serial.print("Temperature: ");
      Serial.print(temperature);
      Serial.print(" *C \n");
    }
    timeSinceLastRead = 0;
  }
  
  delay(100);
  timeSinceLastRead += 100;
  timeSinceGate += 100;
}
